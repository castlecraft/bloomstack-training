import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateNoteCommand } from 'src/note-management/commands/create-note/create-note.command';
import { NoteDto } from 'src/note-management/entities/note/note.dto';

@Controller('note')
export class NoteController {
  constructor(private readonly commandBus: CommandBus) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  createNote(@Body() note: NoteDto) {
    return this.commandBus.execute(new CreateNoteCommand(note));
  }
}
