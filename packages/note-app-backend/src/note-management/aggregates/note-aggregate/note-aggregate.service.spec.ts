import { Test, TestingModule } from '@nestjs/testing';
import { NoteAggregateService } from './note-aggregate.service';

describe('NoteAggregateService', () => {
  let service: NoteAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NoteAggregateService],
    }).compile();

    service = module.get<NoteAggregateService>(NoteAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
