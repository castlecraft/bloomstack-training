import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { NoteCreatedEvent } from 'src/note-management/events/note-created/note-created.event';
import { NoteDto } from 'src/note-management/entities/note/note.dto';
import { Note } from 'src/note-management/entities/note/note.entity';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class NoteAggregateService extends AggregateRoot {
  constructor() {
    super();
  }

  createNote(note: NoteDto) {
    const payload = new Note();
    payload.uuid = uuidv4();
    Object.assign(payload, note);
    this.apply(new NoteCreatedEvent(payload));
  }
}
