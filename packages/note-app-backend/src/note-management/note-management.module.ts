import { Module } from '@nestjs/common';
import { NoteEntityModule } from './entities/note-entity.module';
import { NoteController } from './controllers/note/note.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { NoteCommandManager } from './commands';
import { NoteAggregateManager } from './aggregates';
import { NoteEventsManager } from './events';

@Module({
  imports: [NoteEntityModule, CqrsModule],
  exports: [NoteEntityModule],
  controllers: [NoteController],
  providers: [
    ...NoteCommandManager,
    ...NoteAggregateManager,
    ...NoteEventsManager,
  ],
})
export class NoteManagementModule {}
