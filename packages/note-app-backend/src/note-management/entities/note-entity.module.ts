import { Module } from '@nestjs/common';
import { NoteService } from './note/note.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './note/note.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Note])],
  exports: [NoteService],
  controllers: [],
  providers: [NoteService],
})
export class NoteEntityModule {}
