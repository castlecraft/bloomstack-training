import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { MongoRepository } from 'typeorm';

@Injectable()
export class NoteService {
  constructor(
    @InjectRepository(Note)
    private readonly repository: MongoRepository<Note>,
  ) {}

  async create(payload: Note) {
    return await this.repository.insertOne(payload);
  }
}
