import { NoteCreatedHandler } from './note-created/note-created.handler';

export const NoteEventsManager = [NoteCreatedHandler];
