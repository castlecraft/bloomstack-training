import { ICommand } from '@nestjs/cqrs';
import { NoteDto } from 'src/note-management/entities/note/note.dto';

export class CreateNoteCommand implements ICommand {
  constructor(public note: NoteDto) {}
}
